package com.example.android.miwok;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class NumbersActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.word_list);

        ArrayList<Word> englishNumbers = new ArrayList<Word>();

        //englishNumbers.add("One");
        englishNumbers.add(new Word("one","lutti"));
        //englishNumbers.add("Two");
        englishNumbers.add(new Word("two","otiiko"));
        //englishNumbers.add("Three");
        englishNumbers.add(new Word("three","tolookosu"));
        //englishNumbers.add("Four");
        englishNumbers.add(new Word("four","oyyisa"));
        //englishNumbers.add("Five");
        englishNumbers.add(new Word("five","massokka"));
        //englishNumbers.add("Six");
        englishNumbers.add(new Word("six","temmokka"));
        //englishNumbers.add("Seven");
        englishNumbers.add(new Word("seven","kenekaku"));
        //englishNumbers.add("Eight");
        englishNumbers.add(new Word("eight","kawinta"));
        //englishNumbers.add("Nine");
        englishNumbers.add(new Word("nine","wo e"));
        //englishNumbers.add("Ten");
        englishNumbers.add(new Word("ten","na'aacha"));

        //ArrayAdapter<Word> itemsAdapter = new ArrayAdapter<Word>(this, R.layout.list_item, englishNumbers);
        WordAdapter adapter = new WordAdapter(this, englishNumbers);

        ListView listView = (ListView) findViewById(R.id.list);

        listView.setAdapter(adapter);

    }
}
