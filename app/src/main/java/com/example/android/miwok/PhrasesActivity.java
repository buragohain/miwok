package com.example.android.miwok;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class PhrasesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.word_list);

        ArrayList<Word> englishNumbers = new ArrayList<Word>();

        //englishNumbers.add("One");
        englishNumbers.add(new Word("Where are you going?","minto wuksus"));
        //englishNumbers.add("Two");
        englishNumbers.add(new Word("What is your name?","tinnә oyaase'nә"));
        //englishNumbers.add("Three");
        englishNumbers.add(new Word("My name is...","oyaaset..."));
        //englishNumbers.add("Four");
        englishNumbers.add(new Word("How are you feeling?","michәksәs?"));
        //englishNumbers.add("Five");
        englishNumbers.add(new Word("I’m feeling good.","kuchi achit"));
        //englishNumbers.add("Six");
        englishNumbers.add(new Word("Are you coming?","әәnәs'aa?"));
        //englishNumbers.add("Seven");
        englishNumbers.add(new Word("Yes, I’m coming.","hәә’ әәnәm"));
        //englishNumbers.add("Eight");
        englishNumbers.add(new Word("I’m coming.","әәnәm"));
        //englishNumbers.add("Nine");
        englishNumbers.add(new Word("Let’s go.","yoowutis"));
        //englishNumbers.add("Ten");
        englishNumbers.add(new Word("Come here.","әnni'nem"));

        //ArrayAdapter<Word> itemsAdapter = new ArrayAdapter<Word>(this, R.layout.list_item, englishNumbers);
        WordAdapter adapter = new WordAdapter(this, englishNumbers);

        ListView listView = (ListView) findViewById(R.id.list);

        listView.setAdapter(adapter);
    }
}
