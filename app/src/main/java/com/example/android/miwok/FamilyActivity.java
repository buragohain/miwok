package com.example.android.miwok;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class FamilyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.word_list);

        ArrayList<Word> englishNumbers = new ArrayList<Word>();

        //englishNumbers.add("One");
        englishNumbers.add(new Word("father","әpә"));
        //englishNumbers.add("Two");
        englishNumbers.add(new Word("mother","әṭa"));
        //englishNumbers.add("Three");
        englishNumbers.add(new Word("son","angsi"));
        //englishNumbers.add("Four");
        englishNumbers.add(new Word("daughter","tune"));
        //englishNumbers.add("Five");
        englishNumbers.add(new Word("older brother","taachi"));
        //englishNumbers.add("Six");
        englishNumbers.add(new Word("younger brother","chalitti"));
        //englishNumbers.add("Seven");
        englishNumbers.add(new Word("older sister","teṭe"));
        //englishNumbers.add("Eight");
        englishNumbers.add(new Word("younger sister","kolliti"));
        //englishNumbers.add("Nine");
        englishNumbers.add(new Word("grandmother","ama"));
        //englishNumbers.add("Ten");
        englishNumbers.add(new Word("grandfather","paapa"));

        //ArrayAdapter<Word> itemsAdapter = new ArrayAdapter<Word>(this, R.layout.list_item, englishNumbers);
        WordAdapter adapter = new WordAdapter(this, englishNumbers);

        ListView listView = (ListView) findViewById(R.id.list);

        listView.setAdapter(adapter);
    }
}
