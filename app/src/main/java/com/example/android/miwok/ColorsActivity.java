package com.example.android.miwok;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class ColorsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.word_list);

        ArrayList<Word> englishNumbers = new ArrayList<Word>();

        //englishNumbers.add("One");
        englishNumbers.add(new Word("red","weṭeṭṭi"));
        //englishNumbers.add("Two");
        englishNumbers.add(new Word("green","chokokki"));
        //englishNumbers.add("Three");
        englishNumbers.add(new Word("brown","ṭakaakki"));
        //englishNumbers.add("Four");
        englishNumbers.add(new Word("gray","ṭopoppi"));
        //englishNumbers.add("Five");
        englishNumbers.add(new Word("black","kululli"));
        //englishNumbers.add("Six");
        englishNumbers.add(new Word("white","kelelli"));
        //englishNumbers.add("Seven");
        englishNumbers.add(new Word("dusty yellow","ṭopiisә"));
        //englishNumbers.add("Eight");
        englishNumbers.add(new Word("mustard yellow","chiwiiṭә"));

        //ArrayAdapter<Word> itemsAdapter = new ArrayAdapter<Word>(this, R.layout.list_item, englishNumbers);
        WordAdapter adapter = new WordAdapter(this, englishNumbers);

        ListView listView = (ListView) findViewById(R.id.list);

        listView.setAdapter(adapter);
    }
}
